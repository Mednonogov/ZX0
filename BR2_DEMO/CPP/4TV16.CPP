// W4 hero & misc
//
#include <stdio.h>
#include <alloc.h>
#include <io.h>
#include <fcntl.h>
#include <sys\stat.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
//#include <graphics.h>

#define WID 14
#define HGT 18

char *name=(char*)(malloc(80));

char buff[16384];
char out[HGT][WID][2];
int BMPlen;

//------------------------

void read_f(char *b,int n )
 {
  sprintf(name,"..\\TV16\\%s%04u.bmp",b,n);
  //printf("Open %s\n",name);
  int h=open(name,O_BINARY | O_RDONLY);
  if (h==-1)
  {
    char *errname=(char*)(malloc(70));
    sprintf(errname, "�訡�� ������ 䠩�� (%s)!\7\n", name);
    perror(errname);
    exit(1);
  }
  BMPlen=filelength(h);
  read(h,buff,BMPlen);
  close(h);
 }


//-----------------[conv]-----------------
void conv_f()
{
  int i,j,k;
  long r,g,b,grey1,grey2;
  char *ttb=buff+BMPlen-WID*4*3;
  for (i=0;i<HGT;i++)
  {
    char *tb=ttb;
    for(k=1;k>=0;k--)
    {
      for (j=0;j<WID;j++)
      {
	r=*tb; tb++;
	g=*tb; tb++;
	b=*tb; tb++;
	grey1=15-(b*29L+r*77L+g*150L)/(256L*16L);
	printf("%02X%02X%02X::",r,g,b);
	r=*tb; tb++;
	g=*tb; tb++;
	b=*tb; tb++;
	grey2=15-(b*29L+r*77L+g*150L)/(256L*16L);
	out[i][j][k]=grey2+grey1*16L;
      }
    }
    ttb-=WID*2*2*3;
  }

}

void read_conv_f(char *b,int n1, int n2)
{
 int n;
 char *bb;
 //bb=b;
 bb="tmp_";
 sprintf(name,"..\\TV16\\%s.t16",b);
 int h2=open(name,O_BINARY | O_CREAT | O_RDWR | O_TRUNC,S_IWRITE);
  if (h2==-1){perror("�訡�� c��࠭���� c�ࠩ⮢\7\n");exit(1);}
  for(n=n1;n<n2+1;n++)
  {
    read_f(bb,n);
    conv_f();
    write(h2,out,sizeof(out));
  }
  close(h2);
}


//-------------------------[main]--------------------------------------

int main()
{
 printf("\t���� ��஭-2 (c)1998 M��������� �.�.  \n");
 printf(" ��������� 24bit ���஢ � TV16\n");

 read_conv_f("k_vict0",0,31);
 read_conv_f("k_vict1",32,63);
 read_conv_f("k_vict2",64,95);

 printf("��� yᯥ譮.\n");
 return 0;
}

