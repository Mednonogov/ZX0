// ����� 䠩��� ��ᥬ���� ��� HPLJ5 (��। ����᪮� ������ rusprint.bat)
#include <stdio.h>
#include <alloc.h>
#include <io.h>
#include <fcntl.h>
#include <sys\stat.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <dos.h>

char prn_time[10];
char prn_date[20];

void get_date_time()
{
   struct  time t;
   struct date d;

   gettime(&t);
   sprintf(prn_time,"%02d:%02d:%02d",t.ti_hour, t.ti_min, t.ti_sec);

   getdate(&d);
   sprintf(prn_date,"%2d/%02d/%d", d.da_day,d.da_mon,d.da_year);
   return;
}

int main(int n,char *arg[])
{
#define LF '\n'
#define CR '\r'
#define TAB '\t'
#define LEN 28
#define LINES 96
#define COLUMNS 5
 char *hp5=
"%-12345X@PJL DEFAULT COPIES=1\n"
"%-12345X@PJL DEFAULT ORIENTATION=PORTRAIT\n"
"%-12345X@PJL DEFAULT PAPER=A4\n"
"%-12345X@PJL DEFAULT MANUALFEED=OFF\n"
"%-12345X@PJL DEFAULT FORMLINES=100\n"
"%-12345X@PJL DEFAULT LPARM:PCL PITCH= 20.00\n"
"%-12345X@PJL DEFAULT LPARM:PCL FONTSOURCE=S\n"
"%-12345X@PJL DEFAULT LPARM:PCL FONTNUMBER=1\n"
"%-12345X@PJL DEFAULT ECONOMODE=OFF\n"
"%-12345X@PJL DEFAULT DENSITY=2\n"
"%-12345X@PJL DEFAULT RET=ON\n"
"%-12345X@PJL DEFAULT RESOLUTION=600\n"
"%-12345X@PJL DEFAULT PAGEPROTECT=AUTO\n"
"%-12345X@PJL DEFAULT AUTOCONT=OFF\n"
"%-12345X@PJL DEFAULT IOBUFFER=AUTO\n\0";

 char *b,huge *m,*name;
 char NO=0; //ࠧ�襭�� ����
 char lin[COLUMNS*LINES][LEN];
 unsigned i,j,k;
 printf("\n\t(c)1996 Mednonogov bros.\n");
 printf("\t����� ��ᥬ���஢᪨� 䠩���\n");
 printf("\t(�� ����� �������� rusprint.bat)\n\n");
 get_date_time();
 if (n==1)
	{
	 b=(char*)malloc(60);
 inp:	 printf("�室��� ⥪�⮢� 䠩� : ");
	 gets(b);
	 if (strlen(b)==0) goto inp;
	}
 else b=arg[1];
 for(i=strlen (b);i>0;)
  {
   i--;
   if (b[i]=='\\') break;
   if (b[i]=='.')
     {
      b[i]=0;
      if(b[i+1]!='a' || b[i+2]!='8' || b[i+3]!='0' || b[i+4]!=0)
	{printf("����୮� ���७�� �室���� 䠩��!\n");return -1;}
      break;
     }
  }
 name=(char*)(malloc(70));
 strcpy(name,b);
 strcat(name,".a80");
 int h=open(name,O_BINARY | O_RDONLY);
 if (h==-1){perror("�訡�� ������ ��室���� 䠩��\7\n");return -1;}
 long len=filelength(h);
 if(len==0){printf("���� ����!\n"); return -1;}
 strcpy(name,b);
 strcat(name,".prn");
 FILE *h2=fopen(name, "w");
 if (h2==0){perror("�訡�� ������ 䠩�� ����\7\n");return -1;}
 FILE *prn=fopen("PRN", "w");
 if (prn==0){printf("�ਭ�� �� ������祭\7\n");NO=1;}
 if((m=(char*)malloc(len+1))==0)
  {
   printf("�������筮 ����� (����室��� %u Kb)!\n",len);
   return -1;
  }
 read(h,m,len);
 m[len]=0;
//---------------
 char begin;
 unsigned bl,el;
 char Page=1;
 char huge* pt=m;
 char Cont=0;//0-����� ��ப�, 1-�த�������, 2-����� 䠩��
do
 {
//  fprintf(h2,"\x1B\x52\xF");
  el=Page*LINES*COLUMNS;
  bl=el-LINES*COLUMNS+1;

  fprintf(h2,"\t\t <CopperFeet>  ");
  fprintf(h2," C�࠭��: %u	(%u-%u)    ����: %s    ",Page,bl,el,name);
  fprintf(h2," �६� ����: %s    %s \n\n",prn_time,prn_date);
  for(i=0;i<LINES*COLUMNS;i++)	for(j=0;j<LEN;j++) lin[i][j]=' ';
  i=0;
  do
   {
    begin=1;
    j=0;
    if(1==Cont){begin=0;j=7;lin[i][6]='{';}
    Cont=0;
    while(j<LEN)
     {
      switch (*pt)
       {
	case TAB:
	case ' ': if(begin)
		   {if(j<7) j=7; begin=0;while(*pt==TAB||*pt==' ')pt++;}
		  else *pt=' ';
		  break;
	case '*':
	case ';': begin=0;break;
	case CR: pt++;pt++; goto M1;
	case 0: Cont=2;i++; goto PR;
       }
      lin[i][j]=*pt;
      pt++;j++;
     }
    Cont=1;
M1: i++;
   }
  while (i<LINES*COLUMNS);
  if(*(pt-1)!=LF)Cont=1;
PR:
  char end=(i<LINES?i:LINES);
  for(i=0;i<end;i++)
   {
    for(j=0;j<COLUMNS;j++)
     {
      if(j)fprintf(h2," . ");
      for(k=0;k<LEN;k++) fputc(lin[i+j*LINES][k],h2);
     }
    fprintf(h2,"\n");
   }
  printf("��ࠡ�⠭� ��࠭�� ����� %u. \n",Page);
  fprintf(h2,"\f");
  if(NO==0)
   {
//    printf("������ �NTER ��� ����, ESC ��� �ய�᪠ ��࠭���.\n");
//    do i=getch(); while(i!=27 && i!=13);
//    if(i==27)
	goto N;
//    fprintf(prn,"\x1B\x52\xF");
    fprintf(prn,"\t\t <CopperFeet>  ");
    fprintf(prn," C�࠭��: %u	(%u-%u)    ����: %s    ",Page,bl,el,name);
    fprintf(prn," �६� ����: %s    %s \n\n",prn_time,prn_date);
    for(i=0;i<end;i++)
     {
      for(j=0;j<COLUMNS;j++)
       {
	if(j)fprintf(prn," . ");
	for(k=0;k<LEN;k++) fputc(lin[i+j*LINES][k],prn);
       }
      fprintf(prn,"\n");
     }
    printf("�����⠭� ��࠭�� ����� %u. \n",Page);
    fprintf(prn,"\f");
   }
N:Page++;
 }
 while(Cont!=2);
 printf("O.K.");
 close (h);
 fclose (h2);
 return 0;
}