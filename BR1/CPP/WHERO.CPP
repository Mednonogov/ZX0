// W3 hero & misc
//
#include <stdio.h>
#include <alloc.h>
#include <io.h>
#include <fcntl.h>
#include <sys\stat.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
//#include <graphics.h>

 char buff[6144];
 char hero[192][32];
 char mask[192][32];
 char page[16384];
 char *name=(char*)(malloc(70));
 unsigned pd; //㪠��⥫� � page
 char form1[]={1,0,0,0,1,1,1,255};
 char form2[]={2,2,255};
 char form3[]={1,1,1,1,0,255};
 char form3a[]={0,255};
 char form41[]={4,255};
 char form42[]={4,4,255};
 int py; //⥪�� ��ப� � mask,hero
//----------------------------------------------------------------
void init_page()
 {
  pd=0;
 }

void print_pd(char *mes_pd)
 {
  printf("  %5u  : %s\n",pd,mes_pd);
 }

#define TIFF 194
void read_f1(char *buf,char *b,char *extt )
 {
  char *name=(char*)(malloc(70));
  sprintf(name,"..\\images\\%s%s.tif",b,extt);
  int h=open(name,O_BINARY | O_RDONLY);
  if (h==-1){perror("�訡�� ������ 䠩��!\7\n");exit(1);}
  read(h,buf,TIFF);
  read(h,buf,6144);
  for (int i=0;i<6144;i++) buf[i]^=0xFF;
  close(h);
  py=0;
 }

void read_f(char *b)
 {
  read_f1((char*)hero,b,"");
  read_f1((char*)mask,b,"m");
//  read_f1((char*)mask,b,"d");
 }

void save_page(char *b)
 {
  sprintf(name,"..\\DATA\\%s.dat",b);
  int h2=open(name,O_BINARY | O_CREAT | O_RDWR | O_TRUNC,S_IWRITE);
  if (h2==-1){perror("�訡�� c��࠭���� c�ࠩ⮢\7\n");exit(1);}
  write(h2,page,pd);
  close(h2);
  printf("-----------end of %s---\n",b);
}

//------------------------------------------------------------------

void inv_byte() //⠡��� ������஢���� ���⮢
 {
  int i,j; unsigned char b, inv;
  for (i=0;i<256;i++)
   {
    b=i;inv=0;
    for (j=0;j<8;j++)
     {
      inv<<=1;
      if (b&1) inv|=1;
      b>>=1;
     }
    page[pd]=inv;
    pd++;
   }
  print_pd("end of inv_tab");
 }

void spr2x2()
 {
  int i,j,k;
  for (i=0;i<16;i++)
    for (j=0;j<2;j++)
      for (k=0;k<16;k++)
       {
	char c=mask[py+k][i*2+j];
	page[pd]=c;
	pd++;
	page[pd]=c^hero[py+k][i*2+j];
	pd++;
       }
  py+=16;
 }

void spr2x2ext()
 {
  int i,j,k;
  spr2x2();
  for (i=0;i<8;i++)
    for (j=0;j<2;j++)
      for (k=0;k<16;k++)
       {
	char c=mask[py+k][i*4+j];
	page[pd]=c;
	pd++;
	page[pd]=c^hero[py+k][i*4+j];
	pd++;
       }
  py+=16;
 }

void spr3xN(int shft,int kvo,int wide)
 {
  int i,j,k,l;
    for (i=0;i<kvo;i++)
     for (j=0;j<wide;j++)
      for (k=0;k<24;k++)
       {
	char c=mask[py+k][i*shft+j];
	page[pd]=c;
	pd++;
	page[pd]=c^hero[py+k][i*shft+j];
	pd++;
       }
 }

void spr3x3ext()
 {
  spr3xN(3,8,3);
  py+=48;
  spr3xN(3,8,3);
  py-=24;
  spr3xN(6,4,3);
  py+=48;
  spr3xN(6,4,3);
  py+=24;
 }

void spr2x3ext_compr()
 {
  spr3xN(2,10,2);
  py+=24;
  spr3xN(4,5,2);
  py+=24;
 }

void spr2x3ext()
 {
  spr3xN(2,16,2);
  py+=24;
  spr3xN(4,8,2);
  py+=24;
 }

void conv_f(char *nf)
 {
   while(*nf!=255)
    {
     switch (*nf)
      {
       case 0:{spr2x2();break;}
       case 1:{spr2x2ext();break;}
       case 2:{spr3x3ext();break;}
//	 case 3:{spr2x3ext();break;}
       case 4:{spr2x3ext_compr();break;}
      }
     nf++;
    }
   print_pd("[]");
 }

void conv_f2()
 {
  py=80;
  spr3xN(3,8,3);
  print_pd("end of ���.�ࠩ�� 3�3");
 }

void conv_f1()
{
 int i,j,k,m;
 int ll[2]={16,12};
 py=0;
 for (m=0;m<2;m++)
  {
    for (i=0;i<ll[m];i++)
     for (j=0;j<2;j++)
      for (k=0;k<16;k++)
       {
	char c=mask[py+k][i*2+j];
	page[pd]=c;
	pd++;
	page[pd]=c^hero[py+k][i*2+j];
	pd++;
       }
    py+=16;
  }
 print_pd("end of ���.�ࠩ�� 2�2");

    for (i=0;i<6;i++)
     for (j=0;j<4;j++)
      for (k=0;k<32;k++)
       {
	char c=mask[py+k][i*4+j];
	page[pd]=c;
	pd++;
	page[pd]=c^hero[py+k][i*4+j];
	pd++;
       }
 py+=32;
 print_pd("end of ���.�ࠩ�� 4�4");


  for (i=0;i<18;i++)
    for (k=0;k<8;k++)
       {
	char c=mask[py+k][i];
	page[pd]=c;
	pd++;
	page[pd]=c^hero[py+k][i];
	pd++;
       }
  print_pd("end of ���.�ࠩ�� 1�1");
}


//---------------------------------------------------------------------

int main()
{
 printf("\t���� ��஭ (c)1996 M��������� �.�.  \n");
 printf(" ��������� �ࠩ⮢ ��஥�\n");
// int driv=VGA,mode=VGAHI; initgraph(&driv,&mode,"");
 init_page();
  print_pd("�ࠩ�� ��");
 read_f("whum1");
 conv_f(form1);
 read_f("whum2");
 conv_f(form2);
 save_page("wshum");
//
 init_page();
  print_pd("�ࠩ�� �㭣��");
 read_f("worc1");
 conv_f(form1);
 read_f("worc2");
 conv_f(form2);
 save_page("wsorc");
//
 init_page();
  print_pd("�ࠩ�� ������+�⥭���맠");
 read_f("wcreat2");
 py+=48;
 conv_f(form42);
 save_page("wscreat2");
//
 init_page();
  print_pd("�����.ᮧ�����");
 read_f("wcreat1");
 conv_f(form3);
 read_f("wcreat2");
 conv_f(form41);
 read_f("wmisc");
 conv_f2();
 save_page("wscreat1");
//
 init_page();
 conv_f1();
 save_page("wmisc");
//
 init_page();
  print_pd("�ࠩ�� �ਧ����");
 read_f("wcreat1");
 py+=144;
 conv_f(form3a);
 save_page("wgrizold");
// closegraph();
 printf("��� yᯥ譮.\n");
 return 0;
}

