/* -- ���� ��஭. �஢�ઠ �࠯�� �� ��᪥ 2 -- */
/* ------------- (c) 1997, Copper Feet --------------- */

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include <io.h>
#include <dos.h>
#include <bios.h>
#include <process.h>
#include <fcntl.h>
#include <sys\stat.h>

// ���祭�� ���⭮��/�-�� ��஦�� ��� 0x490=55, 0x491=95
#define DENS_720on1200 0x55
#define DENS_720on1440 0x95

int N_bs=0; //����稪 BAD-ᥪ�஢
int bad_sec[4][3];//0-tr(1-10),1-sec(0-8),2-shift(0-512)
char wr_buff[512];
char rd_buff[512];
char SEC_9[1024]; //���⮢� ᥪ�� ��᪠ 2
char SEC9CMP[1024]; //��� �஢�ન �����
int sec_head[9]; //��������� ᥪ�� ����/��� (0/1)
int sec_data[9]; //⥫� ᥪ�� ��⮥/��� (�/0)
char trk_gap[10]={80,112,90,105,77, 109,84,101,90,104};

char trk_data[] = {
	0, 0, 0x90, 2,
	0, 0, 0x91, 2,
	0, 0, 0x92, 2,
	0, 0, 0x93, 2,
	0, 0, 0x94, 2,
	0, 0, 0x95, 2,
	0, 0, 0x96, 2,
	0, 0, 0x97, 2,
	0, 0, 0x98, 2
		  };

char old_sec_tab[16];
char new_sec_tab[16] =
	{ 00, 00, 00, 2, 0x09, 00,
	  00, 0x50, 0x6f, 00, 00,
	  00,00,00,00,00};

int off_dpt, seg_dpt;
int bios490;
int error;
int DRIVE=0;
int HI;

//--------------------�/�----------------------
void restore() //�����. ������� ⠡���� ��᪥��
 {
  int i;
  for (i=0;i<16;i++) pokeb(seg_dpt,off_dpt+i,old_sec_tab[i]);
  poke(0,0x490,bios490);
  /* ������� Cntrl-Break */
    bdos(0x33, 1, 1);
 }

void set_drv()
 {
  pokeb(0,0x490,DENS_720on1200);
  pokeb(0,0x491,DENS_720on1440);
 }

void store() //c���. ������� ⠡���� ��᪥��
 {
  int i;
  /* �⪫���� Cntrl-Break */
    bdos(0x33, 0, 1);
  /* ������� ⠡��� ��� ��᪥� - ����� 1E */
  off_dpt=peek (0,0x78);
  seg_dpt=peek (0,0x7A);
  for (i=0;i<16;i++)
   {
    old_sec_tab[i]=peekb(seg_dpt,off_dpt+i); /*co��.⠡.��᪥��*/
    if (new_sec_tab[i]!=0) pokeb(seg_dpt,off_dpt+i,new_sec_tab[i]);
//  printf("����_%i\tOld:#%02X	New:#%02X\n", i,old_sec_tab[i],peekb (seg_dpt,off_dpt+i));
   }
  /* ������� ��ࠬ��஢ ���⭮�� */
  bios490=peek(0,0x490);
//  printf ("bios: %04X\n",bios490);
  set_drv();
//  printf ("NOW bios: %04X\n",peek(0,0x490));
 }

int err_exit() //�� ᮮ�� �� �訡�� //���: 1/0-retry/fault
 {
  char *e;
  char c;
  biosdisk(_DISK_RESET,DRIVE,0,0,0,0,NULL);
  switch (error) {
  case 0x01 : e=" ����ୠ� �������";break;
  case 0x02 : e=" ����� ��થ� �� ������";break;
  case 0x03 : e=" ����⪠ ����� �� ������ ���";break;
  case 0x04 : e=" ����� �� ������";break;
  case 0x05 : e=" Reset failed (hard disk)";break;
  case 0x06 : e=" ����� ��᪠ ��᫥ ��᫥���� ����樨";break;
  case 0x07 : e=" Drive parameter activity failed";break;
  case 0x08 : e=" Direct memory access (DMA) overrun";break;
  case 0x09 : e=" Attempt to perform DMA across 64K boundary";break;
  case 0x0A : e=" �����㦥� �訡��� ᥪ��";break;
  case 0x0B : e=" �����㦥�� ����� ��஦��";break;
  case 0x0C : e=" ��ଠ� ��஦�� �� �����ন������";break;
  case 0x10 : e=" �訡�� CRC/ECC �� �⥭�� � ��᪠";break;
  case 0x11 : e=" CRC/ECC corrected data error";break;
  case 0x20 : e=" Controller has failed";break;
  case 0x40 : e=" Seek operation failed";break;
  case 0x80 : e=" ��� �⢥� �� ���ன�⢠ (⠩����)";break;
  case 0xAA : e=" Drive not ready (hard disk only)";break;
  case 0xBB : e=" Undefined error occurred (hard disk only)";break;
  case 0xCC : e=" Write fault occurred";break;
  case 0xE0 : e=" Status error";break;
  case 0xFF : e=" Sense operation failed";break;
  //
  case 0x81 : e=" USER: �訡�� �ࠢ����� ������";break;
  default :e="	*** Unknown error ***";
  }
  printf("��稭�:%s (0x%02X)\t\n\7",e,error);
  printf("������� [R], �ய����� [F], �४���� [E] ?\n");
LOOP2:
  while(!kbhit());
  c=getch();
  switch(c)
  {
   case 'R':
   case 'r': return 1;
   case 'F':
   case 'f': return 0;
   case 'e':
   case 'E': printf("Abort DISK OPERATIONS\n");
	     restore(); textcolor(7); textbackground(0);
	     exit(1);
   default: goto LOOP2;
  }
 }

int isESC() // 1-esc �����
 {
   if(kbhit())
      {
       char c=getch();
       if (c==27)
	{printf("\n�஢�ઠ ��ࢠ�� �� ESC.\n");restore();return 1;}
      }
   return 0;
 }
///////////////////////////////////////////////////
//--------------�������� �����------------------//
///////////////////////////////////////////////////

void main (int argc,char * argv[])
{
  int i,j,k,l,sec_n;
  char c;
  unsigned long blen;

  clrscr();
  textcolor(14);
  textbackground(1);
  cprintf("\n\n�������� ��஭ (c) ���������� �.�.����\n\r");
  cprintf("���������஢�ઠ ᥪ�஢ ��᪠ 2�������\n\r");
  cprintf("���������� �������� ⮫쪮 ��� DOS�����\n\r");

//--������ ��砩�� ���祭��
  randomize();
  for (k=0;k<512;k++)
   wr_buff[i]=random(222)+10;



//------------����� ��᪮���-----------

LOOP:
  textcolor(7);
  printf("\n	    ��᪮��� [A,B,Exit]\n");
LOOP1:
  while(!kbhit());
  c=getch();
  switch(c)
  {
   case 'A':
   case 'a': DRIVE=0;break;
   case 'B':
   case 'b': /*goto LOOP*/;DRIVE=1;break;
   case 'e':
   case 'E': printf("End of DISC OPERATIONS\n");
	     textcolor(7); textbackground(0);
	     exit(0);
   default: goto LOOP1;
  }

  textbackground(0);
  clrscr();
  textbackground(1);
  textcolor(14);
  cprintf("\n\n���������������������������������������������\n\r");
  cprintf("����������� ��஭ (c) ���������� �.�.������\n\r");
  cprintf("������������஢�ઠ ᥪ�஢ ��᪠ 2���������\n\r");
  cprintf("���������������������������������������������\n\r");

  N_bs=0;
//-----------���樠������ ��᪠---------
  store();


FMT0:
  set_drv();
  error=biosdisk(_DISK_RESET,DRIVE,0,0,0,0,NULL);

//----- �஢�ઠ ��஦�� �� �࠯��� ----

  j=1;
  for (i=1;i<=10;i++)
   {
     for(k=0;k<36;k+=4) //ᮧ� ⠡� ᥪ�஢
      {
	trk_data[k]=i;
	trk_data[k+1]=j;
      }
      pokeb(seg_dpt,off_dpt+7,trk_gap[i-1]); // ������� GAP

//--�ଠ�஢����
FMT1:
     set_drv();
     textbackground(0);
     error=biosdisk(_DISK_FORMAT,DRIVE,j,i, 0x90, 9, trk_data);
     if (error==0x06) goto FMT1;
     if (error)
      { printf("\n�訡�� �� �ଠ�஢����!\n"); if(err_exit()) goto FMT1;}
     set_drv();
     cprintf("[Fmt][Fmt][Fmt][Fmt][Fmt][Fmt][Fmt][Fmt][Fmt]  %02u\r",i);

//--������
     for (k=0;k<9;k++)
     {
       if( isESC() ) goto EXI;
       sec_data[k]=1;
       error=biosdisk(_DISK_WRITE,DRIVE,j,i, 0x90+k, 1, wr_buff);
       if (error)
	{ cprintf("[   ]"); sec_head[k]=0; }
       else
	{ cprintf("[---]"); sec_head[k]=1; }
     }
     printf("\r");

//--�⥭�� � �஢�ઠ
#define TRY 6
    for (int t=0; t<TRY;t++) //TRY - �᫮ ����⮪
    {
     int next_trk=1; //1-��३� �� ᫥���騩 �४
     for (k=0;k<9;k++)
     {
      if( isESC() ) goto EXI;
      if(sec_head[k]!=0)
       {
	textbackground(1);
	if(sec_data[k]!=0)
	 {
	  error=biosdisk(_DISK_READ,DRIVE,j,i, 0x90+k, 1, rd_buff);
	  if (!error) goto GOOD_sec;
	  else
	    {
	     //---ᥪ�� ���� - �ࠢ���� �����
	     int ns;
	     for (ns=0;ns<512;ns++)
	       {if(wr_buff[ns]!=rd_buff[ns]) break;}
	     if (ns<30 || ns>450) goto GOOD_sec; //������ � ��� ᥪ��
	     if (t==0)
	       {
		 sec_head[k]=ns;
	       }
	     else
	       {
		// printf("D=%i\n",abs(sec_head[k]-ns));
		 if ( abs(sec_head[k]-ns)>3 ) goto GOOD_sec; // "����⮩稢��" �࠯���
	       }
	     textbackground(4);
	     if(t==TRY-1) cprintf("[-X-]"); else cprintf("[-%u-]",t+1);
	     next_trk=0;
	    }
	 }
	else
	 {
GOOD_sec: cprintf("[---]");
	  sec_data[k]=0;
	 }
       }
      else
       {
	 textbackground(0);
	 cprintf("[   ]");
       }
     }
     printf("\r");
     if(next_trk) break;
    }
    printf("\n");
//----����ᥭ�� � ⠡�. BAD-ᥪ�஢
    for(k=0;k<9;k++)
     {
       if(sec_head[k]!=0 && sec_data[k]!=0)
	{
	  bad_sec[N_bs][0]=i;
	  bad_sec[N_bs][1]=k;
	  bad_sec[N_bs][2]=sec_head[k];
	  N_bs++;
	  if(N_bs>=4)goto srch_END;
	}
     }
   }
//------------------------------------------
  restore();
  textbackground(7);
  textcolor(BLINK+12);
  cprintf("���������������������������������������������\n\r");
  cprintf("���������������� �� ��⠭���� ����배������\n\r");
  cprintf("���������������������������������������������\n\r");
  printf("\7");
EXI:
  goto LOOP;

//-----������ ⠡� ᥪ�஢ � ᥪ�� 9----
srch_END:
//--����� ���⮢� ᥪ��
     HI=open("..\\W02\\DISK2.BIN",O_RDONLY | O_BINARY);
     if(HI==-1) {perror("�訡�� 䠩�� ��ࠧ�! \7");exit(1);}
     read(HI,SEC_9,1024);
     close(HI);
//---���� ����
     SEC_9[0]-=8;

//--- 2-�����, 111-�.䠩���, 16-⠡� ���� ᥪ�஢
//0-7-⠡� (sssstttt-ᥪ��(0-8)/�४(1-10); ᬥ饭��_�࠯���/2)

//���饭�� ⠡���� �� ��砫� ���⮢��� ᥪ��
#define dTABL 113
  for(k=0;k<4;k++)
    {
      SEC_9[dTABL+k*2]=bad_sec[k][0]+bad_sec[k][1]*16;
      SEC_9[dTABL+k*2+1]=bad_sec[k][2]/2;
//	printf("s/t:%X	shft:%u  ",SEC_9[dTABL+k*2],SEC_9[dTABL+k*2+1]);
    }
//���⠢��� ����� ⠡����--------
//8=(2)-(1)
      SEC_9[dTABL+8]  =SEC_9[dTABL+2]-SEC_9[dTABL+1];
//9=add(0..7)+31
      char sum=31;
      for (k=0;k<8;k++) sum+=SEC_9[dTABL+k];//printf("s=%02X  %02X\n",sum,SEC_9[dTABL+k]);}
      SEC_9[dTABL+9]  =sum;
//10=(3)+63
      SEC_9[dTABL+10] =SEC_9[dTABL+3]+63;
//11=(4)+(5)+48
      SEC_9[dTABL+11] =SEC_9[dTABL+4]+SEC_9[dTABL+5]+48;
//12=(9)+(11)
      SEC_9[dTABL+12] =SEC_9[dTABL+9]+SEC_9[dTABL+11];
//13=128
      SEC_9[dTABL+13] =128;
//14=sec_tabl(14)
      SEC_9[dTABL+14] =SEC_9[84];
//15=(7)-(6)-1
      SEC_9[dTABL+15] =SEC_9[dTABL+7]-SEC_9[dTABL+6]-1;

//      printf("\n");
//      for(i=0;i<16;i++) printf("[%u]=%2X ",i,SEC_9[dTABL+i] );
//      printf("\n");
//������� ⠡����----------------
WTR9:
   i=0;j=0;
   pokeb(seg_dpt,off_dpt+3,3); // ������� ����� 3
   for (k=0;k<12/*5*/;k++)
    {
     set_drv();
     error=biosdisk(_DISK_WRITE,DRIVE,j,i, 0x9, 1, SEC_9);
     if (error)
      { printf("\n�訡�� �� ����� sec #9!\n"); if(err_exit()) goto WTR9;}
    }
//�஢���� sec9-----------------
READ9:
   i=0;j=0;
   pokeb(seg_dpt,off_dpt+3,3); // ������� ����� 3
   for (k=0;k<6/*5*/;k++)
    {
     set_drv();
     error=biosdisk(_DISK_READ,DRIVE,j,i, 0x9, 1, SEC9CMP);
     if (error)
      { printf("\n�訡�� �� �⥭�� sec#9!\n"); if(err_exit()) goto READ9;}
     for(l=0;l<1024;l++)
      {
//	 if(l<256)printf("adr:%3u  %3X\n",l,SEC_9[l]);
       if (SEC_9[l]!=SEC9CMP[l])
	 {
	   printf("\n�訡�� ������ sec#9!\n");
	   error=0x81;
	   if(err_exit()) goto WTR9;
	 }
      }

    }

  restore();
  textbackground(1);
  textcolor(15);
  cprintf("���������������������������������������������\n\r");
  cprintf("�����������***���� ��⠭������***����������\n\r");
  cprintf("���������������������������������������������\n\r");
  goto LOOP;
}
